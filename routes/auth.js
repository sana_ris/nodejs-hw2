const express = require('express');
const router = express.Router();
const authcontroller = require('../controllers/auth');
const authmiddleware = require('../middleware/auth');
const { register, login } = require('../controllers/auth');

const { check } = require('express-validator');
router.post(
  '/auth/register',
  [
    check('username', 'Username can not be empty').notEmpty(),
    check(
      'password',
      'Password must be greater than 4 and led than 10 symbols'
    ).isLength({ min: 4, max: 10 }),
  ],
  register
);
router.post('/auth/login', login);

module.exports = router;
