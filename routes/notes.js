const express = require('express');
const router = express.Router();
const authmiddleware = require('../middleware/auth');
const notescontroller = require('../controllers/notes');

router.post('/notes', authmiddleware, notescontroller.createNote);
router.get('/notes/:id', authmiddleware, notescontroller.getNotes);
router.get('/notes/:id', authmiddleware, notescontroller.getSingleNote);
router.delete('/notes/:id', authmiddleware, notescontroller.deleteNote);
router.patch('/notes/:id', authmiddleware, notescontroller.checkNote);

module.exports = router;
