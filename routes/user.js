const express = require('express');
const router = express.Router();
const { getProfileInfo } = require('../controllers/user');
const usermiddleware = require('../middleware/user');

router.get('/users/me', usermiddleware, getProfileInfo);

module.exports = router;
