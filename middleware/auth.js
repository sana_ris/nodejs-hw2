const jwt = require('jsonwebtoken');
const secretkey = proces.env.SECRET_KEY;

module.exports = function (req, res, next) {
  if (req.method === 'OPTIONS') {
    next();
  }

  try {
    const token = req.headers.authorization.split(' ')[1];
    if (!token)
      return res.status(400).send({ message: 'You are not authorised' });
    const decodedData = jwt.verify(token, secretkey);
    req.user = decodedData;
    next();
  } catch (error) {
    console.log(error);
    return res.status(400).send({ message: 'Client error' });
  }
};
