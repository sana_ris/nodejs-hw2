const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');
const db = process.env.PORT || 8080;

require('dotenv').config();

const authRouter = require('.routes/auth');

app.use('/api', authRouter);
app.use(express.json());
app.use(morgan(':method :status URL::url "HTTP/:http-version"'));
app.use(cors());

mongoose
  .connect(db, { useNewUrlParser: true })
  .catch((err) => console.log(err));

app.listen(PORT);
