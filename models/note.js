const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const noteSchema = new Schema(
  {
    userId: {
      type: String,
      trim: true,
    },
    text: {
      type: String,
      required: true,
    },
    completed: {
      type: Boolean,
      default: false,
      required: true,
    },
    createdDate: {
      type: Date,
      default: Date.now(),
      required: true,
    },
  },
  { versionKey: false }
);

const Note = mongoose.model('Note', noteSchema);

module.exports = Note;
