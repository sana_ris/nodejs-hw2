const Profile = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const secretkey = proces.env.SECRET_KEY;
const { validationResult } = require('express-validator');

const generateAccessToken = (id, username) => {
  const payload = {
    id,
    username,
  };
  return jwt.sign(payload, secretkey, { expiresIn: '24h' });
};

const login = async (req, res) => {
  try {
    const { username, password } = req.body;
    const user = await Profile.findOne({ username });
    const validPassword = bcrypt.compare(password, user.password);
    const token = generateAccessToken(user._id, user.username);
    if (!user) {
      return res.status(400).send({ message: 'User does not exist' });
    }
    if (!validPassword) {
      return res.status(400).send({ message: 'Invalid password' });
    }
    return res.status(200).send({ message: 'Success', jwt_token: token });
  } catch (error) {
    res.status(500).send({ message: 'Server error' });
  }
};

const register = (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ message: 'Registration error' });
    }
    const { username, password } = req.body;
    const hashPassword = bcrypt.hashSync(password, 7);
    const user = new Profile({ username, password: hashPassword });
    user
      .save()
      .then(() => res.status(200).send({ message: 'Success' }))
      .catch((err) => res.status(400).send({ message: 'Client error' }));
  } catch (err) {
    res.status(500).send({ message: 'Server error' });
  }
};

module.exports = {
  login,
  register,
};
