const Profile = require('../models/user');

const getProfileInfo = async (req, res) => {
  try {
    const user = await Profile.findOne({ username: req.user.username });
    const { _id, username, createdDate } = user;
    return res.status(200).send({
      user: {
        _id,
        username,
        createdDate,
      },
    });
  } catch (err) {
    res.status(500).send({ message: 'Server error' });
  }
};
module.exports = {
  getProfileInfo,
};
