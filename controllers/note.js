const Note = require('../models/note');

class notesController {
  createNote = (req, res) => {
    try {
      const { text } = req.body;
      const { id } = req.user;
      const note = new Note({ text, userId: id });
      note
        .save()
        .then(() => res.status(200).send({ message: 'Success' }))
        .catch((error) => {
          console.log(error);
          res.status(400).send({ message: 'Client error' });
        });
    } catch (error) {
      res.status(500).send({ message: 'Server error' });
    }
  };

  getSingleNote = (req, res) => {
    try {
      const id = req.params.id;
      Note.findById(id)
        .then((note) => res.status(200).send({ note: note }))
        .catch((error) => res.status(400).send({ message: 'Client error' }));
    } catch (error) {
      res.status(500).send({ message: 'Server error' });
    }
  };

  getNotes = (req, res) => {
    try {
      const { offset, limit } = req.query;
      const perPage = Math.max(0, +limit);
      Note.find()
        .skip(+offset || 0)
        .limit(+limit || 0)
        .then((notes) =>
          res.status(200).send({
            offset: +offset || 0,
            limit: +limit || 0,
            count: Math.floor(perPage / +offset || 0),
            notes: notes,
          })
        )
        .catch((error) => res.status(400).send({ message: 'Client error' }));
    } catch (error) {
      res.status(500).send({ message: 'Server error' });
    }
  };

  deleteNote = (req, res) => {
    try {
      const id = req.params.id;
      Note.findByIdAndDelete(id)
        .then(() => res.status(200).send({ message: 'Success' }))
        .catch((error) => res.status(400).send({ message: 'Client error' }));
    } catch (error) {
      res.status(500).send({ message: 'Server error' });
    }
  };

  checkNote = (req, res) => {
    try {
      Note.findById(req.params.id)
        .then((note) =>
          note
            .updateOne(
              note.completed === false
                ? { completed: true }
                : { completed: false }
            )
            .then(() => res.status(200).send({ message: 'Success' }))
            .catch((error) => res.status(400).send({ message: 'Client error' }))
        )
        .catch((error) => res.status(400).send({ message: 'Client error' }));
    } catch (error) {
      res.status(500).send({ message: 'Server error' });
    }
  };
}

module.exports = new notesController();
